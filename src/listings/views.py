from django.shortcuts import render
from django.views.generic import ListView, DetailView

from .models import Listing
from listings.choices import bedroom_choices, price_choices, state_choices


class ListingView(ListView):
    model = Listing
    template_name = 'listings/listings.html'
    paginate_by = 1

    def get_queryset(self):
        qs = super(ListingView, self).get_queryset()
        return qs.filter(is_published=True)


class ListDetailView(DetailView):
    model = Listing
    template_name = 'listings/listing.html'


class SearchView(ListView):
    model = Listing
    template_name = 'listings/search.html'

    def get_queryset(self):
        object_list = super(SearchView, self).get_queryset()

        # Keywords
        if 'keywords' in self.request.GET:
            keywords = self.request.GET['keywords']
            if keywords:
                object_list = object_list.filter(
                    description__icontains=keywords)

          # City
        if 'city' in self.request.GET:
            city = self.request.GET['city']
            if city:
                object_list = object_list.filter(city__iexact=city)

        # State
        if 'state' in self.request.GET:
            state = self.request.GET['state']
            if state:
                object_list = object_list.filter(state__iexact=state)

        # Bedrooms
        if 'bedrooms' in self.request.GET:
            bedrooms = self.request.GET['bedrooms']
            if bedrooms:
                object_list = object_list.filter(bedrooms__lte=bedrooms)

        # Price
        if 'price' in self.request.GET:
            price = self.request.GET['price']
            if price:
                object_list = object_list.filter(price__lte=price)

        return object_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bedroom_choices'] = bedroom_choices
        context['price_choices'] = price_choices
        context['state_choices'] = state_choices
        context['values'] = self.request.GET
        return context
