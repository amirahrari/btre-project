from django.shortcuts import render
from django.views.generic import ListView

from realtors.models import Realtor
from listings.models import Listing
from listings.choices import bedroom_choices, price_choices, state_choices


class IndexPage(ListView):
    model = Listing
    template_name = 'pages/index.html'

    def get_queryset(self):
        qs = super(IndexPage, self).get_queryset()
        return qs.filter(is_published=True)[:3]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bedroom_choices'] = bedroom_choices
        context['price_choices'] = price_choices
        context['state_choices'] = state_choices
        return context


class AboutPage(ListView):
    model = Realtor
    template_name = 'pages/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['mvp_realtors'] = Realtor.objects.all().filter(is_mvp=True)
        return context
